from django.apps import AppConfig


class DateAutomationsConfig(AppConfig):
    name = 'date_automations'
