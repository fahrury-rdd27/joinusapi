# Generated by Django 3.1.6 on 2021-05-20 01:15

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RegStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(choices=[('UNGGULAN', 'UNGGULAN'), ('REGULER', 'REGULER'), ('DIPLOMA', 'DIPLOMA')], default=None, max_length=20)),
                ('date_open', models.DateField(default=datetime.date.today)),
                ('date_closed', models.DateField()),
                ('status', models.BooleanField(default=True)),
            ],
        ),
    ]
