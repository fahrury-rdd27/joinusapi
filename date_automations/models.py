from django.db import models
from datetime import date

class RegStatus(models.Model):
    # define scholarship types
    TYPE_CHOICES = [
        ("UNGGULAN", "UNGGULAN"),
        ("REGULER", "REGULER"),
        ("DIPLOMA", "DIPLOMA"),
    ]

    type = models.CharField(
        max_length= 20,
        choices= TYPE_CHOICES, 
        default= None,
        blank=False
    )
    date_open = models.DateField(default=date.today)
    date_closed = models.DateField()
    status_open = models.BooleanField(default=True)

    def __str__(self):
        return self.type