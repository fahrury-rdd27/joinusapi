from django.db import models
from rest_framework import serializers
from .models import RegStatus

class RegStatusSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'type', 'date_open', 'date_closed', 'status_open',)
        model = RegStatus