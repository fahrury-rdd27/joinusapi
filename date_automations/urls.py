from django.urls import path
from .views import RegStatusDetail, RegStatusList

urlpatterns = [
    path('', RegStatusList.as_view()),
    path('<int:pk>/', RegStatusDetail.as_view()),
]