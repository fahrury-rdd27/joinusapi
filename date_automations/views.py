from django.db.models.fields import NullBooleanField
from django.shortcuts import render
from .models import RegStatus
from .serializers import RegStatusSerializer
from rest_framework import generics

class RegStatusList(generics.ListAPIView):
    queryset = RegStatus.objects.all()
    serializer_class = RegStatusSerializer

class RegStatusDetail(generics.RetrieveUpdateAPIView):
    if id:
        queryset = RegStatus.objects.all()
        serializer_class = RegStatusSerializer